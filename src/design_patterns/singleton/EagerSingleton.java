package design_patterns.singleton;
 class EagerSingleton {
    //Only object created when class is loaded and theInstance is private static var pointing to it.
    private static EagerSingleton theInstance = new EagerSingleton();

    //private constructor
    private EagerSingleton(){
    	System.out.println("created");
    }
    //public method to return single instance of class .
    public static EagerSingleton getInstance(){
        return theInstance;
    }
}

