package design_patterns.factory.shapes;

public interface GeometricShape {
    void draw();
}