package design_patterns.decorator;

public class Cream extends CondimentDecorator{

	public Cream(Beverage beverage) {
		super(beverage);
		
	}

	@Override
	public String getDescription() {
		return beverage.getDescription()+", Cream";
	}

	@Override
	public double getPrice() {
		return beverage.getPrice() + 5;
	}

}
