package design_patterns.decorator;



import design_patterns.decorator.Beverage;
import design_patterns.decorator.CondimentDecorator;

public class Milk  extends CondimentDecorator{

	public Milk(Beverage beverage) {
		super(beverage);
		
	}

	@Override
	public String getDescription() {
		
		return beverage.getDescription()+", Milk";
	}

	@Override
	public double getPrice() {
		return beverage.getPrice() + 3;
	}

}
