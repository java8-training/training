package design_patterns.decorator;

public interface Beverage { 
    String getDescription(); 
    double getPrice(); 
} 