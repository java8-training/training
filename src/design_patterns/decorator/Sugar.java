package design_patterns.decorator;

public class Sugar extends CondimentDecorator {

	
	public Sugar(Beverage beverage) {
		super(beverage);
		
	}

	public double getPrice() {
		return beverage.getPrice() + 1;
	}

	@Override
	public String getDescription() {
		
		return beverage.getDescription()+", Sugar";
	}
}