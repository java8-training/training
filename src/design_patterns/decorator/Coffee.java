package design_patterns.decorator;

public class Coffee implements Beverage {

	@Override
	public String getDescription() {

		return "Coffee";
	}

	@Override
	public double getPrice() {
		return 10;
	}
}
