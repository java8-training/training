package design_patterns.decorator;



public class Main {

	public static void main(String[] args) {

		 Beverage beverage= new Coffee();
		 beverage = new Milk(beverage);
		 beverage = new Sugar(beverage);
		 beverage = new Sugar(beverage);
		 
		// Beverage beverage= new Sugar(new Milk(new Coffee()));
		
		System.out.println("Description: " +beverage.getDescription());
		System.out.println("Price: " +beverage.getPrice());
	}

}
