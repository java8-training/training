package java_latest.streams.collect;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class NamesExample {
	public static void main(String[] args) {

		List<String> names = Arrays.asList("jon snow", "arya stark", "bran stark");

		//Using lambda
		List<String> upperCasesNames1=names.stream()
			.map(name->name.toUpperCase())
			.collect(Collectors.toList());
		
		System.out.println(upperCasesNames1);
		
		
		//Using Method references
		List<String> upperCasesNames2=names.stream()
			.map(String::toUpperCase)
			.collect(Collectors.toList());
		
		System.out.println(upperCasesNames2);
		
		
		//Converting to Linked List 
		List<String> upperCasesNames3=names.stream()
					.map(String::toUpperCase)
					.collect(Collectors.toCollection(LinkedList::new));
				
				System.out.println(upperCasesNames3);
	
	}
}
