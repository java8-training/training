package java_latest.streams.sort;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import common.Gender;
import common.Person;

public class SortPersons {

	public static void main(String[] args) {
		
		List<Person> listPersons = new ArrayList<>();
		 
		listPersons.add(new Person("Alice", "Brown", "alice@gmail.com", Gender.FEMALE, "1986-05-17"));
		listPersons.add(new Person("Bob", "Young", "bob@gmail.com", Gender.MALE, "1976-08-17"));
		listPersons.add(new Person("Carol", "Hill", "carol@gmail.com", Gender.FEMALE, "1956-05-27"));
		listPersons.add(new Person("David", "Green", "david@gmail.com", Gender.MALE, "1989-01-27"));
		listPersons.add(new Person("Eric", "Young", "eric@gmail.com", Gender.MALE, "1975-02-07"));
		listPersons.add(new Person("Frank", "Thompson", "frank@gmail.com", Gender.MALE, "1952-04-07"));
		listPersons.add(new Person("Gibb", "Brown", "gibb@gmail.com", Gender.MALE, "1995-07-17"));
		listPersons.add(new Person("Henry", "Baker", "henry@gmail.com", Gender.MALE, "1999-09-12"));
		listPersons.add(new Person("Isabell", "Hill", "isabell@gmail.com", Gender.FEMALE, "1970-08-12"));
		listPersons.add(new Person("Jane", "Smith", "jane@gmail.com", Gender.FEMALE, "1969-05-14"));
		
		
		System.out.println("-----------Sorting on lastname Using Lambda---------");
		listPersons.stream()
		.sorted((p1,p2)->p1.getLastName().compareToIgnoreCase(p2.getLastName()))
		.forEach(System.out::println);
		
		
		System.out.println("-----------Sorting on age Using Lambda and Comparator.comparing---------");
		listPersons.stream()
		.sorted(Comparator.comparing(person->person.getAge()))
		.forEach(System.out::println);
		
		
		System.out.println("-----------Sorting on age Using Method reference and Comparator.comparing---------");
		listPersons.stream()
				.sorted(Comparator.comparing(Person::getAge))
				.forEach(System.out::println);
		
		System.out.println("-----------Sorting on age Using Method reference and Comparator.comparing and in Descending---------");
		listPersons.stream()
				.sorted(Comparator.comparing(Person::getAge).reversed())
				.forEach(System.out::println);
		
		
	}

}
