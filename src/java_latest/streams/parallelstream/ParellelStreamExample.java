package java_latest.streams.parallelstream;

import java.util.Arrays;
import java.util.List;

public class ParellelStreamExample {
public static void main(String... args) {
		
		List<String> names = Arrays.asList("jon snow", "arya stark", "bran stark");

		names.parallelStream()
			.forEach(name -> System.out.println(name));
		

	}
}
