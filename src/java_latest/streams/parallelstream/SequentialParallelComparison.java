package java_latest.streams.parallelstream;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.stream.Stream;

public class SequentialParallelComparison {

    public static void main (String[] args) {
        String[] strings = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};

        System.out.println("-------\nRunning sequential\n-------");
        LocalDateTime startTime=LocalDateTime.now();
        run(Arrays.stream(strings));
        System.out.println("-------\nSequential Duration: "+Duration.between(startTime, LocalDateTime.now()));
        startTime=LocalDateTime.now();
        System.out.println("-------\nRunning parallel\n-------");
        run(Arrays.stream(strings).parallel());
        System.out.println("-------\nParellel Duration: "+Duration.between(startTime, LocalDateTime.now()));
        
    }

    public static void run (Stream<String> stream) {

        stream.forEach(s -> {
            System.out.println(LocalTime.now() + " - value: " + s +
                                " - thread: " + Thread.currentThread().getName()+" Started");
            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(LocalTime.now() + " - value: " + s +
            					" - thread: " + Thread.currentThread().getName()+" Completed");
        });
    }
}
