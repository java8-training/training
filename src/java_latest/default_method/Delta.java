package java_latest.default_method;
interface Alpha{
	   
	   public default void display() {
	      System.out.println("display method of alpha");
	   }
	}
interface Beta{
	   
	   public default void display() {
	      System.out.println("display method of beta");
	   }
	}
	interface Gamma{
	   
	   public default void display() {
	      System.out.println("display method of gamma");
	   }
	}
	
	public class Delta implements Beta, Gamma{
	   public void display() {
		  Beta.super.display();
	   }
	   public static void main(String args[]) {
	      Delta obj = new Delta();
	      obj.display();
	      
	   }
	}