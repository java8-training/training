package java_latest.static_method.basic_example;

class AnyClass implements InterfaceWithStaticMethod
{
    @Override
    public void abstractMethod() 
    {
        System.out.println("Abstract Method implemented");
    }
         
}