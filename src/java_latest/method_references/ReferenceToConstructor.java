package java_latest.method_references;

interface Messageable {
	Message getMessage(String msg);
}

class Message {
	Message(String msg) {
		System.out.print(msg);
	}
}

public class ReferenceToConstructor {
	public static void main(String[] args) {
		//Reference to Constructor 
		//syntax- ContainingClass::new 
		//Alternate for Messageable hello = message->new Message(message);
		
		
		Messageable hello = Message::new;
		hello.getMessage("Hello");
	}
}
