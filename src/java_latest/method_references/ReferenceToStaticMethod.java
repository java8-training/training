package java_latest.method_references;

interface Sayable {
	void say();
}

public class ReferenceToStaticMethod {
	public static void saySomething() {
		System.out.println("Hello, this is static method.");
	}

	public static void main(String[] args) {
		
		//Reference to a Static Method 
		//syntax- ContainingClass::staticMethodName 
		//Alternate for Sayable sayable = ()->ReferenceToStaticMethod.saySomething();
		
		Sayable sayable = ReferenceToStaticMethod::saySomething;
		
		sayable.say();
	}
}
